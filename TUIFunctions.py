import fileUtil as fU
import otherUtil as oU
from webUtil import isValidSite as iVS
import numpy as np
from time import ctime as ct
from os import path
from os.path import isdir
from os import chdir
from os import mkdir
from os import getcwd
from shutil import rmtree
import inspect
import sys
import Tkinter, tkFileDialog


class TUIFunctions():

	exitOptions = """1) Yes
	2) No
	"""

	se_options = """1) Edit Both Dates
	2) Edit the Start Date
	3) Edit the End Date
	4) Return to the Menu
	"""

	save_DIR = setNewDefultStartDIR()
	siteToCapture = "https://web.archive.org/"
	maxCapNum = 99999999999999999999 # Arbertary Large Number
	startDate = (1, 1, 1970)
	endDate = rT(gTS(True))

	def setNewDefultStartDIR(self):
		currentSaveDIR = fU.get_script_dir()
		if not os.path.isdir(currentSaveDIR + "\Output"):
			fU.createDIR("Output", False, currentSaveDIR)
		else:
			fU.dumpFiles(currentSaveDIR + "\Output", False)
		return currentSaveDIR + "\Output"

	def setNewSaveDIR(self):
		root = Tkinter.Tk()
		root.withdraw()
		newSaveDIR = tkFileDialog.askdirectory(parent=root,initialdir="/",title='Please select a directory')
		return newSaveDIR

	def setNewSite(self, first, question):
		if first == 1 or first == 6:
			print(question)
			oU.iBR()
			# print(options)
			# oU.iBR()
			print("Enter input:")
			inputToReturn = input(">>> ")
			if first != 1:
				first += 1
			else:
				first = 1
		else:
			print("Enter input:")
			inputToReturn = input(">>> ")
			first += 1
		if oU.iVS(inputToReturn):
			print("That site does not exist.")
			print("Please enter a site that exists.")
			oU.iBR()
			setNewSite(first, question)
		else:
			return inputToReturn

	def getMaxCaptureNum(self, first):
		if first == 1 or first == 6:
			print("""Please note that this option will be ignored if you 
			specify a start and end date. also note that if you 
			set a maximum that is above the highest number of captures 
			for that site the program will just use the default.
			""")
			oU.iBR()
			print("Please enter the maximum number of captures you would like to make.")
			oU.iBR()
			print("Enter input:")
			inputToReturn = input(">>> ")
			if first != 1:
				first += 1
			else:
				first = 1
		else:
			print("Enter input:")
			inputToReturn = input(">>> ")
			first += 1
		if inputToReturn > 99999999999999999999:
			print("If I'm honest I didn't think that you would see this message.")
			print("But hay, the number that you entered is too big. It needs to")
			print("less than 20 9's in a row.")
			print("Please enter a number thats less than that.")
			oU.iBR()
			getMaxCaptureNum(first)
		else:
			return inputToReturn

	def getOptionValues(oNum):
		if oNum == 0:
			return (save_DIR, siteToCapture, maxCapNum, startDate, endDate)
		elif oNum == 1:
			return save_DIR
		elif oNum == 2:
			return siteToCapture
		elif oNum == 3:
			return maxCapNum
		elif oNum == 4:
			return (startDate, endDate)
		else:
			return None

	def outputCurrentOptionsValues():
		print("Current Save Directory: ", save_DIR)
		print("Current Site to Capture: ", siteToCapture)
		print("Current Maximum Number of Captures: ", maxCapNum)
		print("Current Start Date: ", startDate)
		print("Current End Date: ", endDate)
		oU.iBR()

	def setSE_date(operation): #, incTime
		if operation == "start" or operation == "end":
			dd = oU.getInput(1, "What is day of the date that you would like to ", operation, " at?", " ", None)
			mm = oU.getInput(1, "What is month of the date that you would like to ", operation, " at?", " ", None)
			yyyy = oU.getInput(1, "What is year of the date that you would like to ", operation, " at?", " ", None)
			if oU.isValidDate(dd, mm, yyyy):
				print("That is not a valid date, please enter a valid date.")
				setSE_date(operation) #, incTime
			return dd, mm, yyyy
		else:
			whichDate = oU.getInput(1, "What would you like to do with the start and end dates?", se_options, ("1", "2", "3", "4"))
			incTime = oU.getYN_answer("Would you like to edit the time values?")
			if whichDate == 1:
				return setSE_date("start"), setSE_date("end")
				# edit both dates
			elif whichDate = 2:
				return setSE_date("start")
				# edit start date
			elif whichDate = 3:
				return setSE_date("end")
				# edit end date
			else:
				return None
			# else:
			# 	if incTime:
			# 		hh = oU.getInput(1, "What is the hour on the date that you would like to start at?", " ", None)
			# 		mm = oU.getInput(1, "What is the hour on the date that you would like to start at?", " ", None)

	def closeProgram(self):
		exitI = ("1", "2")
		checkExit = ou.getInput(1, "Are you sure you want to quit?", exitOptions, exitI)
		if checkExit = "1":
			oU.closeAllScripts()

# "Please enter the name of the site you would like to "
