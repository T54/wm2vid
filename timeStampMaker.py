import time as t
import sys
import subprocess

initText = """Program by:

       _         _______       _           
      /\ \      / ___  /\  _  /\ \         
      \_\ \    / /\__\ \ \/\_\\ \ \        
      /\__ \  / / /   \_\/ / / \ \ \       
     / /_ \ \/ / /      / / /   \ \ \      
    / / /\ \ \ \ \      \ \ \____\ \ \     
   / / /  \/_/\ \ \      \ \________\ \    
  / / /        \ \ \      \/________/\ \   
 / / /     ____/ / /                \ \ \  
/_/ /     /_____/ /                  \ \_\ 
\_\/      \_____\/                    \/_/

Program info, info on me, legal and info on 
bug reporting can be found here:
https://goo.gl/mNfcKj
"""

def createTime(asArray):
	ct = t.ctime()
	ctMonth = ct[4] + ct[5] + ct[6]
	months = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
	for i in range(0, 11):
		if ctMonth == months[i]:
			if i + 1 < 10:
				ct = ct[:4] + str(0) + str(i+1) + ct[-16:]
			else:
				ct = ct[:4] + str(i+1) + ct[-16:]
			break
	ct = ct[-18:]
	ct = ct.replace(" ", "")
	ct = ct.replace(":", "")
	if asArray:
		retArray = (ct[2] + ct[3], ct[0] + ct[1], ct[-4:], ct[4:10])
		return retArray
	else:
		ct = ct[2] + ct[3] + ct[0] + ct[1] + ct[-4:] + ct[4:10]
		return ct

def copy2clip(txt):
	cmd='echo '+txt.strip()+'|clip'
	return subprocess.check_call(cmd, shell=True)

def startUp():
	print (initText)

def outStamp():
	timestamp = createTime(False)
	print (timestamp)
	print (" ")
	print ("Above is your generated timestamp.")
	return timestamp
	
def main():
	timestamp = outStamp()
	todo = " "
	count = 0
	while todo != "E":
		print ("What would you like to do with your timestamp?")
		print ("C = Copy it to clipboard")
		print ("N = Generate New timestamp")
		print ("I = Display program information")
		print ("E = Exit")
		todo = input(">>> ")
		todo = todo.upper()
		if todo == "C":
			copy2clip(timestamp)
			print (" ")
			print ("Timestamp " + timestamp + " copied to clipboard.")
			print (" ")
		elif todo == "N":
			print (" ")
			timestamp = outStamp()
		elif todo == "I":
			print (" ")
			startUp()
		elif todo == "E":
			sys.exit()
		elif todo == "CONVERT":
			cnum = int(input())
			cnum = cnum + 1
			cnum = str(cnum)
			signature = "T.F.T.C. - T54 - " + timestamp + " - " + cnum + " Caches found."
			copy2clip(signature)
		else:
			if count >= 5:
				print ("The string you entered is not C, N, I or E.")
				print ("Please enter C, N, I or E.")
				print (" ")
				print (timestamp)
				print (" ")
				print ("Above is your generated timestamp.")
				count  = 0
			else:
				print ("The string you entered is not C, N, I or E.")
				print ("Please enter C, N, I or E.")
				print (" ")
			count = count + 1

if __name__ == "__main__":
	startUp()
	main()
