# from selenium import webdriver
# import numpy as np
# import time as t
# import os
# from django.core.validators import URLValidator
# from django.core.exceptions import ValidationError
from otherUtil import insertBlankReturn as ibr
from otherUtil import getTimeStamp as gTS
from otherUtil import removeTime as rT
from otherUtil import getInput
import TUIFunctions as TUIF

initText = """Program by:

       _         _______        _           
      /\ \      / ___  /\  _   /\ \         
      \_\ \    / /\__\ \ \/\_\ \ \ \        
      /\__ \  / / /   \_\/ / /  \ \ \       
     / /_ \ \/ / /      / / /    \ \ \      
    / / /\ \ \ \ \      \ \ \_____\ \ \     
   / / /  \/_/\ \ \      \ \_________\ \    
  / / /        \ \ \      \/_________/\ \   
 / / /     ____/ / /                 \ \ \  
/_/ /     /_____/ /                   \ \_\ 
\_\/      \_____\/                     \/_/ 

Program info, info on me, legal and info on 
bug reporting can be found here:
https://goo.gl/mNfcKj

"""

UIInfo = """Please note that operations will not occer if you 
have not entered a start date and end date. The 
start and end dates are the first capture into 
the wayback machine and todays last capture by defult.

The number of captures per day is the maximum for the 
day that is being processed by defult. If you set a 
mazimum that is above the highest number of captures 
for that site the program will just use the defult.

By defult this program will output to the \'Output\' 
folder in the direcotry that you have installed the 
program in.
"""

textUI = """    1) Change output directory
   2) Set website to capture.
   3) Set maximum number of captures per site.
   4) Set start date and end date.
   5) Start operation
   6) Exit
"""

class textUserInterface():

	# save_DIR = TUIF.setNewDefultStartDIR()
	# siteToCapture = "https://web.archive.org/"
	# maxCapNum = 9999999999999999999 # Arbertary Large Number
	# startDate = (1, 1, 1970)
	# endDate = rT(gTS(True))

	def main():
		question = "Please select one of the following options by entering its respective number."
		options = textUI
		validInputs = {1, 2, 3, 4, 5, 6}
		if not mainSeen:
			startUp(False)
		else:
			startUp(True)
		ibr()
		selectedOption = otherUtil.getInput(1, question, options, validInputs)
		if selectedOption == 1:
			TUIF.setNewSaveDIR()
		elif selectedOption == 2:
			question = "Please enter the name of the site you would like to screenshot"
			TUIF.setNewSite(1, question)
		elif selectedOption == 3:
			TUIF.getMaxCaptureNum(1)
		elif selectedOption == 4:
			TUIF.setSE_date()
		elif selectedOption == 5:
			TUIF.outputCurrentOptionsValues()
		elif selectedOption == 6:
			TUIF.closeProgram()
		else:
			main()	

	def startUp(SUSeen):
		if not SUSeen:
			print (initText)
			print (UIInfo)
		else:
			print (UIInfo)
		

	#driver = webdriver.Firefox()
	#driver.get('http://www.python.org/')
	#driver.save_screenshot('python_org.png')
	#driver.quit()

if __name__ == "__main__":
	main()
