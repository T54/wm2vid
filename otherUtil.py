from selenium import webdriver
import numpy as np
from time import ctime as ct
import time
from os import path
from timeStampMaker import createTime, copy2clip
from sys import exit as e

class otherUtil():

	def insertBlankReturn(self):
		print(" ")

	def getTimeStamp(self, asArray):
		return createTime(asArray)

	def tsToArray(self, ts):
		dd = ts[:2]
		mm = ts[2:4]
		yyyy = ts[4:8]
		if ts.len == 14:
			time = ts[:-6]
			dateAsArray = (dd, mm, yyyy, time)
		else:
			dateAsArray = (dd, mm, yyyy)
		return dateAsArray

	def arrayToTs(self, arr):
		if arr.len == 2:
			return arr[0] + arr[1] + arr[2]
		else:
			return arr[0] + arr[1] + arr[2] + arr[3]

	def removeTime(self, ts):
		if type(ts) is not str:
			ts.remove(3)
		else: 
			return ts[:-6]

	def copyTTC(self, txt):
		return copy2clip(txt)

	# WBTS = WayBack TimeStamp
	def processWBTS(self, wBTS):
		return wBTS[6:8] + wBTS[4:6] + wBTS[:4] + wBTS[-6:]

	def getInput(self, first, question, options, validInputs):
		if first == 1 or first == 6:
			print(question)
			ibr()
			print(options)
			ibr()
			print("Enter input:")
			inputToReturn = input(">>> ")
			if first == 1:
				first += 1
			else:
				first = 1
		else:
			print("Enter input:")
			inputToReturn = input(">>> ")
		first += 1
		if inputToReturn not in validInputs:
			print("That is not a valid input.")
			print("Please make a valid input.")
			ibr()
			getInput(first, question, options, validInputs)
		elif validInputs is None:
			return inputToReturn
		else:
			return inputToReturn
			
	def getYN_answer(question):
		options = """1) Yes
		2) No
		"""
		binAns = getInput(1, question, options ("1", "2"))
		if binAns == 1:
			return True
		else:
			return False
	
	def isValidDate(day, month, year):
		this_date = '%d/%d/%d' % (month, day, year)
		try:
			time.strptime(this_date, '%m/%d/%Y')
		except ValueError:
			return False
		else:
			return True

	def isValidTime(hh, mm, ss):
		if hh < 24 and mm < 60 and ss < 60:
			return True
		else:
			return False

	def strArrToIntArr(self, strArr):
		for object in strArr:
			strArr[object] = int(strArr[object])
		return strArr

	def closeAllScripts(self):
		sys.e()