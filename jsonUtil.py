from webUtil import isValidSite as iVS
import json
import urllib.request

class jsonUtil():

	def getJSON(self, passURL):
		with urllib.request.urlopen(passURL) as url:
			data = url.read()
		return data

	def parseJSON(self, data):
		return json.loads(data.decode())

	def encodeJSON(self, data):
		return json.dumps(data)